import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import VideoRoomComponent from './components/VideoRoomComponent';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<VideoRoomComponent  openviduServerUrl="https://ec2-18-182-62-246.ap-northeast-1.compute.amazonaws.com"/>, document.getElementById('root'));
registerServiceWorker();
